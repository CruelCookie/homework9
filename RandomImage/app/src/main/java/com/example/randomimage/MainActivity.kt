package com.example.randomimage

import android.graphics.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var imViewer: ImageView
    private lateinit var btChanger: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imViewer = findViewById(R.id.imViewer)
        btChanger = findViewById(R.id.btChanger)
        val pic = listOf(R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5, R.drawable.img6, R.drawable.img7)

        imViewer.setBackgroundResource(pic[3])
        btChanger.setOnClickListener { // Change to a random picture from the list
            val rand = Random.nextInt(pic.size)
            imViewer.setBackgroundResource(pic[rand])

            // Change to a random RGB color
            val color =
                Color.argb(255, Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
            btChanger.setBackgroundColor(color)
        }
    }
}